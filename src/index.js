import Vue from 'vue'
import router from './router'
import App from './App.vue';

// import App from './components/App' // 作ったやつ

// uikitの読み込み
import UIkit from 'uikit'
import Icons from 'uikit/dist/js/uikit-icons'
import 'uikit/dist/css/uikit.css'
import 'uikit/dist/css/uikit.min.css'
UIkit.use(Icons);

// グローバルコンポーネントの読み込み
// Alert

new Vue({

	el: '#app', // アプリをマウントする要素(セレクタで指定)

	router,

	render: h => h(App)
})
